package com.dt76.wmsConsumer.controller;


import com.dt76.wmsConsumer.remote.test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class testController {

    @Autowired
    private test testRemote;

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        System.out.println("尝试远程调用");
        return testRemote.Test();
    }
}
