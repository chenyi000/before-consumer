package com.dt76.wmsConsumer.controller;

import com.dt76.wmsConsumer.pojo.SysCustomer;
import com.dt76.wmsConsumer.rabbitMq.MailSendUtil;
import com.dt76.wmsConsumer.rabbitMq.MailService;
import com.dt76.wmsConsumer.remote.LoginRemote;
import com.dt76.wmsConsumer.utils.AccountUtil;
import com.dt76.wmsConsumer.utils.CommonReturnType;
import com.dt76.wmsConsumer.utils.CustomerInfoUtil;
import com.dt76.wmsConsumer.utils.MD5Utils;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 15:41
 */

@Controller
public class LoginController {

    @Autowired
    private LoginRemote loginRemote;

    @Autowired
    private MailService mailService;


    @RequestMapping("accountLogin")
    public String login(AccountUtil accountUtil, HttpSession session) {
        accountUtil.setPassword(MD5Utils.md5(accountUtil.getPassword()));
        SysCustomer loginCustomer = loginRemote.login(accountUtil);
        if (loginCustomer != null) {
            session.setAttribute("loginCustomer", loginCustomer);
            return "index";
        }
        return "login";
    }


    @RequestMapping("regiset")
    public String regiset(SysCustomer sysCustomer) {
        Gson gson = new Gson();
        sysCustomer.setCPassword(MD5Utils.md5(sysCustomer.getCPassword()));
        CommonReturnType regist = loginRemote.regist(gson.toJson(sysCustomer));
        if(regist == null){
            return null;
        }
        if(regist.getStatus().equals("success")){
            return "login";
        }
        return "regiset";
    }


    @RequestMapping("getCode")
    @ResponseBody
    public CommonReturnType getCode(@RequestParam("email") String email) {

        Random random = new Random();
        String code = String.valueOf(random.nextInt(6) + 1000000);


        //组织邮箱发送信息
        List<String> emails = new ArrayList<>();
        emails.add(email);
        //中间件发送邮件
        MailSendUtil cont = new MailSendUtil("注册验证码", code, emails);
        mailService.sendEmail(cont);
        return CommonReturnType.create(code);
    }

    @RequestMapping("")
    public String toLogin() {
        return "login";
    }


    @RequestMapping("/toindex")
    public String toIndex() {
        return "index";
    }


    @RequestMapping("/toRegister")
    public String toRegister() {
        return "regiset";
    }


}
