package com.dt76.wmsConsumer.controller;


import com.dt76.wmsConsumer.pojo.SysCustomer;
import com.dt76.wmsConsumer.rabbitMq.MailSendUtil;
import com.dt76.wmsConsumer.rabbitMq.MailService;
import com.dt76.wmsConsumer.rabbitMq.OrderMqService;
import com.dt76.wmsConsumer.remote.OrderMessageRemote;
import com.dt76.wmsConsumer.utils.*;
import com.google.gson.Gson;
import com.netflix.ribbon.proxy.annotation.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * 客户下订单
 */

@Controller
@RequestMapping("/ordershop")
public class OrderMessageController {

    @Autowired
    private OrderMessageRemote orderMessageRemote;

    @Autowired
    private OrderMqService orderMqService;

    @Autowired
    private MailService mailService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 下订单
     *
     * @param orderShop
     * @return
     */
    @RequestMapping("sendORder")
    public String sendOrder(SendOrderUtil orderShop, HttpSession session) {
        System.out.println(orderShop.toString());

        Gson gson=new Gson();

        //产生订单号
        //1生成订单号
        String orderId = "DT" + WmsUUID.getUUID();
        orderShop.setOrderId(orderId);

        //写入redis
        stringRedisTemplate.opsForValue().set(orderId,gson.toJson(orderShop));

        System.out.println(stringRedisTemplate.opsForValue().get(orderId));
        //TODO 插入中间发送订单信息到王欣
        orderMqService.sendOrder(orderShop);

        SysCustomer customer = (SysCustomer) session.getAttribute("loginCustomer");

        //组织邮箱发送信息
        List<String> emails = new ArrayList<>();
        emails.add(customer.getCEmail());
        //中间件发送邮件
        MailSendUtil cont = new MailSendUtil("小顺丰物流", "尊敬的客户，您的订单已签收，订单号："+orderId+"!", emails);


        mailService.sendEmail(cont);


        return "index";
    }

    /*
     *//**
     * 展示订单
     *//*
    @RequestMapping("findOrder")
    public CommonReturnType findOrder(@RequestParam("pageIndex")String pageIndex, HttpSession session){
        SysCustomer customer=(SysCustomer)session.getAttribute("currrentUser");
        PageUitl order = orderMessageRemote.findOrder(customer.getCId(), pageIndex);
        if(order != null){
            return CommonReturnType.create(order);
        }
        return CommonReturnType.create("查询失败","fail");
    }*/


    /**
     * 查看订单详细信息
     */
    @RequestMapping("findOrder")
    @ResponseBody
    public CommonReturnType findOrderMessage(@RequestParam("orderId") String orderId) {
        OrderMessageShowUtil orderMessage = orderMessageRemote.findOrderMessage(orderId);
        if (orderMessage != null) {
            return CommonReturnType.create(orderMessage);
        }
        return CommonReturnType.create("未找到相应的订单号", "fail");
    }

    /**
     * 签收订单
     */
    @RequestMapping("accptOrQuitOrder")
    @ResponseBody
    public CommonReturnType accptOrQuitOrder(@RequestParam("status") String status,
                                             @RequestParam("orderId") String orderId) {

        CommonReturnType result = null;
        if (status.equals("签收")) {
            result = orderMessageRemote.accptOrQuitOrder(orderId, "18");
        } else if (status.equals("退货")) {
            result = orderMessageRemote.accptOrQuitOrder(orderId, "13");
        }
        if (result != null) {
            return result;
        }
        return CommonReturnType.create("操作异常", "fail");
    }


    /**
     * 退订订单
     */

    @RequestMapping("/quitOrder")
    @ResponseBody
    public CommonReturnType quitOrder(QuitOrderUtil quitOrderUtil) {

        System.out.println(quitOrderUtil.toString());
        //写入redis
        //TODO 中间件发送订单信息到褚子期
        orderMqService.quitOrder(quitOrderUtil);
        return CommonReturnType.create("退订成功");
    }
}
