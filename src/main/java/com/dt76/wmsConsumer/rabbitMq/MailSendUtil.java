package com.dt76.wmsConsumer.rabbitMq;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/9/1.
 */
@Getter
@Setter
@Component
public class MailSendUtil implements Serializable {

    private String subject;  //主题
    private String content;  //内容
    private List<String> emialAddress; //邮箱地址

    public MailSendUtil(String subject, String content, List<String> emialAddress) {
        this.subject = subject;
        this.content = content;
        this.emialAddress = emialAddress;
    }

    public MailSendUtil() {
    }
}