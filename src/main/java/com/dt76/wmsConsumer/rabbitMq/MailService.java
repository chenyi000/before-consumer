package com.dt76.wmsConsumer.rabbitMq;

/**
 * @Description
 * @auther jun
 * @create 2019-05-16 18:06
 */
public interface MailService {


    void sendEmail(MailSendUtil content);
}
