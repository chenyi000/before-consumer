package com.dt76.wmsConsumer.rabbitMq;


import com.google.gson.Gson;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @auther jun
 * @create 2019-05-16 16:27
 */

@Service
public class MailServiceImpl implements MailService {

    //注入工具类
    @Autowired
    private AmqpTemplate rabbitMqTemplate;

    private Gson gson =new Gson();

    public void sendEmail(MailSendUtil content){
        rabbitMqTemplate.convertAndSend("mail",gson.toJson(content));
    }

}
