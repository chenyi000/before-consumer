package com.dt76.wmsConsumer.rabbitMq;

import com.dt76.wmsConsumer.utils.QuitOrderUtil;
import com.dt76.wmsConsumer.utils.SendOrderUtil;
import com.google.gson.Gson;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @auther jun
 * @create 2019-05-16 16:27
 */

@Service
public class OrderMqServiceImpl implements OrderMqService {

    //注入工具类
    @Autowired
    private AmqpTemplate rabbitMqTemplate;

    private Gson gson =new Gson();


    /**
     * 发送订单
     *
     * @param orderMessage
     */
    @Override
    public void sendOrder(SendOrderUtil orderMessage) {
        String s = gson.toJson(orderMessage);
        rabbitMqTemplate.convertAndSend("orderSend",s);
    }

    /**
     * 退订签收订单
     *
     * @param quitOrderUtil
     */
    @Override
    public void quitOrder(QuitOrderUtil quitOrderUtil) {
        rabbitMqTemplate.convertAndSend("quitOrder",gson.toJson(quitOrderUtil));
    }
}
