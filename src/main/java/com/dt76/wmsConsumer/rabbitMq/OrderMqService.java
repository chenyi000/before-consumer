package com.dt76.wmsConsumer.rabbitMq;


/**
 * @Description
 * @auther jun
 * @create 2019-05-16 18:06
 */

import com.dt76.wmsConsumer.utils.QuitOrderUtil;
import com.dt76.wmsConsumer.utils.SendOrderUtil;

/**
 * 向后台发送order 信息
 */
public interface OrderMqService {

    /**
     * 发送订单
     * @param orderMessage
     */
    void sendOrder(SendOrderUtil orderMessage);

    /**
     * 退订签收订单
     * @param quitOrderUtil
     */
    void quitOrder(QuitOrderUtil quitOrderUtil);



}
