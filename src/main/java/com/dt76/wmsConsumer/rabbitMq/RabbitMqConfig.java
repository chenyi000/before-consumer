package com.dt76.wmsConsumer.rabbitMq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//spring 自定义配置
@Configuration
public class RabbitMqConfig {

    //自定义bean:bean的id是方法名queue
   /* @Bean
    public Queue queue() {
        return new Queue("mail");
    }

    @Bean
    public Queue queue1() {
        return new Queue("phone");
    }*/

    @Bean
    public Queue queue2() {
        return new Queue("orderSend");
    }
    @Bean
    public Queue queue3() {
        return new Queue("orderAccpt");
    }
    @Bean
    public Queue queue4() {
        return new Queue("orderQuit");
    }




   /* @Bean
    public Queue exQueue1() {
        return new Queue("topic.ex_queue_1");
    }

    @Bean
    public Queue exQueue2() {
        return new Queue("topic.ex_queue_2");
    }

    //交换机
    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("topicExchange");
    }

    //绑定器
    @Bean
    Binding bindingExQueue1(Queue exQueue1, TopicExchange topicExchange) {
        return BindingBuilder.bind(exQueue1).to(topicExchange).with("topic.ex_queue_1");
    }

    @Bean
    Binding bindingExQueue2(Queue exQueue2, TopicExchange topicExchange) {
        return BindingBuilder.bind(exQueue2).to(topicExchange).with("topic.#");
    }

    //广播模式
    @Bean
    public Queue queueA() {
        return new Queue("fanout.A");
    }

    @Bean
    public Queue queueB() {
        return new Queue("fanout.B");
    }

    @Bean
    public Queue queueC() {
        return new Queue("fanout.C");
    }

    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanoutExchange");
    }

    @Bean
    Binding bindingFanoutQueueA(Queue queueA, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueA).to(fanoutExchange);
    }

    @Bean
    Binding bindingFanoutQueueB(Queue queueB, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueB).to(fanoutExchange);
    }

    @Bean
    Binding bindingFanoutQueueC(Queue queueC, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueC).to(fanoutExchange);
    }*/
}
