package com.dt76.wmsConsumer.remote;


import com.dt76.wmsConsumer.hystrix.OrderMessageHystrix;
import com.dt76.wmsConsumer.utils.CommonReturnType;
import com.dt76.wmsConsumer.utils.OrderMessageShowUtil;
import com.dt76.wmsConsumer.utils.SendOrderUtil;
import com.dt76.wmsConsumer.utils.PageUitl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "wms-before-service", fallback = OrderMessageHystrix.class)
public interface OrderMessageRemote {


   /* *//**
     * 展示订单
     *//*
    @RequestMapping("/ordershop2/findOrder")
    PageUitl findOrder(@RequestParam("customerid") Long customerId);
*/

    /**
     * 查看订单详细信息
     */
    @RequestMapping("/ordershop2/findOrder")
    OrderMessageShowUtil findOrderMessage(@RequestParam("orderId") String orderId);

    /**
     * 签收/订单退订
     */
    @RequestMapping("/ordershop2/accptOrQuitOrder")
    CommonReturnType accptOrQuitOrder(@RequestParam("orderId") String orderId,
                               @RequestParam("status") String status);
}
