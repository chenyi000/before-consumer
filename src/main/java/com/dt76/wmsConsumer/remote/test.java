package com.dt76.wmsConsumer.remote;

import com.dt76.wmsConsumer.hystrix.testHystrix;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "wms-before-service",fallback = testHystrix.class)
public interface test {

    @RequestMapping("/test")
    String Test();
}
