package com.dt76.wmsConsumer.remote;

import com.dt76.wmsConsumer.hystrix.LoginRemoteHystrix;
import com.dt76.wmsConsumer.pojo.SysCustomer;
import com.dt76.wmsConsumer.utils.AccountUtil;
import com.dt76.wmsConsumer.utils.CommonReturnType;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 15:37
 */

@FeignClient(name = "wms-before-service",fallback = LoginRemoteHystrix.class)
public interface LoginRemote {


    @RequestMapping("accountLogin2")
    SysCustomer login(@RequestBody AccountUtil accountUtil);


    @RequestMapping("regiset2")
    CommonReturnType regist(@RequestParam("syscustomer") String syscustomer);
}
