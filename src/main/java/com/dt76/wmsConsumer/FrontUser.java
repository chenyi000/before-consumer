package com.dt76.wmsConsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
//启用服务的被注册中心发现--自己主动注册到注册中心去
@EnableDiscoveryClient
//启用feign客户端支持
@EnableFeignClients
//启用HystrixDashboard
@EnableHystrixDashboard
//启用CircuitBreaker
@EnableCircuitBreaker
public class FrontUser {

    public static void main(String[] args) {
        SpringApplication.run(FrontUser.class, args);
    }

}
