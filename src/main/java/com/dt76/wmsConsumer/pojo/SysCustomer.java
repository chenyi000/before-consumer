package com.dt76.wmsConsumer.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysCustomer implements Serializable {



  private long cId;
  private String cCode;
  private String cUsername;
  private String cPassword;
  private String cNickname;
  private String cRealname;
  private String cEmail;
  private long cCompanyId;
  private String cTelphone;
  private String cAddress;
  private long cCustomerType;
  private long cStatus;
  private java.util.Date cCreateTime;
  private String cCreatName;
  private java.util.Date cUpdateTime;
  private String cUpdateName;

}
