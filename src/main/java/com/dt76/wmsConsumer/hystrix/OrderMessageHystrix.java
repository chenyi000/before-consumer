package com.dt76.wmsConsumer.hystrix;

import com.dt76.wmsConsumer.remote.OrderMessageRemote;
import com.dt76.wmsConsumer.utils.CommonReturnType;
import com.dt76.wmsConsumer.utils.OrderMessageShowUtil;
import com.dt76.wmsConsumer.utils.SendOrderUtil;
import com.dt76.wmsConsumer.utils.PageUitl;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 22:59
 */
@Component
public class OrderMessageHystrix implements OrderMessageRemote {



    /**
     * 查看订单详细信息
     *
     * @param orderId
     */
    @Override
    public OrderMessageShowUtil findOrderMessage(String orderId) {
        return null;
    }

    /**
     * 签收/订单退订
     *
     * @param orderId
     * @param status
     */
    @Override
    public CommonReturnType accptOrQuitOrder(String orderId, String status) {
        return null;
    }
}
