package com.dt76.wmsConsumer.hystrix;

import com.dt76.wmsConsumer.remote.test;
import org.springframework.stereotype.Component;

@Component
public class testHystrix implements test {

    @Override
    public String Test() {
        System.out.println("熔断机制生效");
        return "sorry ,the remote service is down > not connected >>>>";
    }
}
