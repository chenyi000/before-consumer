package com.dt76.wmsConsumer.hystrix;

import com.dt76.wmsConsumer.pojo.SysCustomer;
import com.dt76.wmsConsumer.remote.LoginRemote;
import com.dt76.wmsConsumer.utils.AccountUtil;
import com.dt76.wmsConsumer.utils.CommonReturnType;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 15:39
 */
@Component
public class LoginRemoteHystrix implements LoginRemote {

    @Override
    public SysCustomer login(AccountUtil accountUtil) {
        return null;
    }

    @Override
    public CommonReturnType regist(String syscustomer) {
        return null;
    }
}
