package com.dt76.wmsConsumer.utils;

/**
 * @Description
 * @auther jun
 * @create 2019-05-09 11:14
 */

import java.io.Serializable;

/**
 * 通用放回值
 */
public class CommonReturnType implements Serializable {

    //表明对应请求的放回处理结果success 或者 fail

    private String status;
    //若status = success 则data内放回前端休要的json数据
    //若status =fail 则放回通用的错误码
   private Object data;

   //定义一个通用的创建方法
    public static CommonReturnType create(Object reult){
        return CommonReturnType.create(reult,"success");
    }

    public static CommonReturnType create(Object result,String status){
        CommonReturnType type=new CommonReturnType();
        type.setStatus(status);
        type.setData(result);
        return type;
    }

    public static void regist(String syscustomer) {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
