package com.dt76.wmsConsumer.utils;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 *@program: u3
 *@description: 前台订单信息接收类
 *@author: wx
 *@create: 2019-05-10 08:46
 */
@Data
@Component
public class SendOrderUtil implements Serializable {


    private String orderId;//订单号
    private String cusName;//客户姓名
    private String masName;//收件人姓名
    private String cusAddress;//客户地址
    private String masAddress;//收件人地址
    private String cusPhone;//客户电话
    private String masPhone;//收件人电话
    private String typeId;//订单类型
    private String shpName;//商品名称
    private String shpType;//商品类型
    private String shpNum;//商品数量
    private String shpWeight;//商品重量
    private String shpTiji;//商品体积
    private String inType;//存放类型
    private String proDate;//生产日期
    private String baozhiqi;//保质期
    private String unit;//商品单位
    private Integer inDay;//存放时间
    private String remark;//备注
}
