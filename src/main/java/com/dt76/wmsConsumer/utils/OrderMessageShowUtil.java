package com.dt76.wmsConsumer.utils;


import lombok.Data;

/**
 * 前台订单展示信息
 */
@Data
public class OrderMessageShowUtil {

    private Long id;
    private String orderCode;
    private String shopName;
    private Integer status;
    private String statusStr;

}
