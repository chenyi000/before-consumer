package com.dt76.wmsConsumer.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 23:17
 */

@Data
public class PageUitl implements Serializable {

    private Integer start;
    private Integer pageIndex;
    private Integer pageSumCount;
    private Integer pageSize=10;
    private Integer pageNum;

    private Object data;

    public PageUitl(Integer start, Integer pageIndex, Integer pageSumCount, Integer pageNum, Object data) {
        this.start = start;
        this.pageIndex = pageIndex;
        this.pageSumCount = pageSumCount;
        this.pageNum = pageNum;
        this.data = data;
    }

    public PageUitl(Integer start, Integer pageIndex, Integer pageSumCount, Integer pageSize, Integer pageNum, Object data) {
        this.start = start;
        this.pageIndex = pageIndex;
        this.pageSumCount = pageSumCount;
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.data = data;
    }

    public PageUitl() {
    }
}
