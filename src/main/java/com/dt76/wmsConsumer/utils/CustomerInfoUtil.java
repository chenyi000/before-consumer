package com.dt76.wmsConsumer.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @auther jun
 * @create 2019-05-14 21:44
 */

@Data
public class CustomerInfoUtil implements Serializable {

    private String cRealname;
    private String cTelphone;
    private String cAddress;
    private Long cCustomerType;
    private Long status;

    private  String customerType;
    private String statusName;

    private String pName;
    private String pPerson;
    private String pContacts;
    private String pTelphone;
    private String pEmail;
    private String pAddress;
}
