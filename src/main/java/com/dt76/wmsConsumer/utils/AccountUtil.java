package com.dt76.wmsConsumer.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 16:55
 */
@Data
public class AccountUtil implements Serializable {

    private String loadname;
    private String password;
}
